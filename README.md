# Tris
Tris è un semplice programma scritto in Java che permette di giocare al popolarissimo gioco.
## Funzionamento
Il codice, scritto in Java utilizzando la libreria grafica Swing, permette a due giocatori di scontrarsi in una partita di tris.
## Installazione
Scaricare il codice sorgente e compilare da linea di comando (CMD, Shell) utilizzando la Java Virtual Machine (Richiesta JavaSE 7 o superiore)