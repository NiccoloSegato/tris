package it.aldini.AI;

import java.util.ArrayList;

public class Tree {

    Node root;
    int livello;

    public Tree() {
        this.root = null;
        livello = 0;
    }

    public Tree(Node n) {
        root = n;
        root.parent = null;
        livello = 0;
    }

    public Node getNode(Integer livello, Integer posizione){

        // Scorro fino al livello voluto
        Node tmp = root.getFirstChild();
        for (int i = 1; i < livello; i++){
            tmp = tmp.getFirstChild();
        }

        // Prendo i fratelli e scorro a quello voluto
        ArrayList<Node> lista_bro = tmp.listBrother;
        return lista_bro.remove(posizione - 0);

    }

    public void addNode(int liv, Node n) {
        if ((liv == 0) && (root == null)) {
            root = n;
            root.parent = null;
            livello = 0;
            return;
        }
        if (liv == 0) {
            Node tmp;
            tmp = root;
            n.firstChild = tmp;
            tmp.parent = n;
            root = n;
            livello++;
            return;
        }

        int i = 0;
        Node tmp;
        tmp = root;
        if ((livello >= liv)) {
            while (i < liv) {
                tmp = tmp.getFirstChild();
                i++;
            }
            tmp.setBrother(n);
        }
        else if (livello + 1 == liv) {
            while (i < livello) {
                tmp = tmp.getFirstChild();
                i++;
            }
            tmp.setFirstChild(n);
            livello++;
        }

    }

    /*
    public String printTree() {
        Node tmp = root;
        return toStringSS(tmp);


    }*/

    /*
    public String toStringSS(Node n) {
        System.out.println("sono qui");
        String s = "";
        if (n.val == null) return " ";

        if (n.listBrother != null) {

            System.out.println(n.listBrother.size());
            int i = 0;
            while (i < n.listBrother.size()) {
                System.out.println(" sono qui 3");
                System.out.println(n.listBrother.get(i));
                s = s + toStringSS(n.listBrother.get(i));
                i++;
            }
        }
        if (n.firstChild == null) {
            System.out.println("sono qui 2");
            return n.val;


        }


        return n.val + s + toStringSS(n.firstChild);

    }

    public int calcolaA() {
        Node v = root;
        return height(root, 0, 0);
    }

    public int height(Node v, int hmax, int h) {
        if (v.firstChild == null) return 0;
        Node w = v.firstChild;
        hmax = height(w, hmax, h);
        int i = 0;
        if (w.listBrother != null) {
            while (i < w.listBrother.size()) {
                h = height(w.listBrother.get(i), hmax, h);
                if (h > hmax) hmax = h;
                i++;
            }
        }
        return 1 + hmax;
    }*/
}
