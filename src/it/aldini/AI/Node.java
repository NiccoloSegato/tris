package it.aldini.AI;

import java.util.ArrayList;

public class Node {

    // region ATTRIBUTI
    Integer numero_casella;
    Integer casella_succ;
    Node firstChild;
    Node parent;
    ArrayList<Node> listBrother;
    // endregion

    // region COSTRUTTORE
    public Node(Integer val, Integer casella_succ) {
        this.numero_casella = val;
        firstChild = null;
        parent = null;
        listBrother = null;
        this.casella_succ = casella_succ;
    }
    // endregion

    // region GET & SET
    public Integer getVal() {
        return numero_casella;
    }

    public void setVal(Integer val) {
        this.numero_casella = val;
    }

    public Node getFirstChild() {
        return firstChild;
    }

    public void setFirstChild(Node firstChild) {
        this.firstChild = firstChild;
        firstChild.parent = this;
    }

    public Node getParent() {
        return parent;
    }

    public ArrayList<Node> getListBrother() {
        return listBrother;
    }

    public void setBrother(Node nt) {
        if (listBrother == null) {
            listBrother = new ArrayList<Node>();
            listBrother.add(0, nt);
        }
        else {
            listBrother.add(nt);
            nt.parent = this.parent;
        }
    }

    public Integer getNumero_casella() {
        return numero_casella;
    }

    public void setNumero_casella(Integer numero_casella) {
        this.numero_casella = numero_casella;
    }

    public Integer getCasella_succ() {
        return casella_succ;
    }

    public void setCasella_succ(Integer casella_succ) {
        this.casella_succ = casella_succ;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setListBrother(ArrayList<Node> listBrother) {
        this.listBrother = listBrother;
    }
    // endregion

}
