package it.aldini.tris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public abstract class Vincita {

    public static JFrame window = new JFrame("Fine");
    private static Container cont = window.getContentPane();
    private static SpringLayout layout = new SpringLayout();
    private static JPanel panel = new JPanel();

    private static final String IMG_PATH_WIN = "src/images/win_red.jpg";

    private static final Color rosso = new Color(208,85,75);

    public static void drawVincita(String vincitore){

        Main.window.dispatchEvent(new WindowEvent(Main.window, WindowEvent.WINDOW_CLOSING));
        window.setBounds(100, 100, 500, 520);
        panel.setBackground(rosso);
        panel.setLayout(layout);

        ImageIcon background = new ImageIcon(IMG_PATH_WIN);
        JLabel label = new JLabel();
        label.setBounds(100, 100, 500, 500);
        label.setIcon(new ImageIcon(background.getImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT)));
        panel.add(label);

        JLabel scrittaVincita = new JLabel(vincitore + " wins!");
        panel.add(scrittaVincita);

        layout.putConstraint(SpringLayout.NORTH, label, 100, SpringLayout.NORTH, cont);
        layout.putConstraint(SpringLayout.WEST, label, 100, SpringLayout.WEST, cont);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, scrittaVincita, window.getWidth()/2, SpringLayout.WEST, cont);
        layout.putConstraint(SpringLayout.NORTH, scrittaVincita, 400, SpringLayout.NORTH, cont);

        cont.add(panel);
        window.setVisible(true);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void drawPareggio(){
        Main.window.dispatchEvent(new WindowEvent(Main.window, WindowEvent.WINDOW_CLOSING));
        window.setBounds(100, 100, 500, 520);
        cont.setBackground(Color.YELLOW);
        window.setVisible(true);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
