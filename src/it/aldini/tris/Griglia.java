package it.aldini.tris;
import it.aldini.trisController.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

abstract class Griglia {

    private static AIController AI;

    private static Container cont = Main.window.getContentPane();
    private static SpringLayout layout = new SpringLayout();
    private static JPanel panel = new JPanel();

    private static JButton uno = new JButton("          ");
    private static JButton due = new JButton("  ");
    private static JButton tre = new JButton("   ");
    private static JButton quattro = new JButton("    ");
    private static JButton cinque = new JButton("     ");
    private static JButton sei = new JButton("      ");
    private static JButton sette = new JButton("       ");
    private static JButton otto = new JButton("        ");
    private static JButton nove = new JButton("         ");

    private static final JButton[][] matrice = new JButton[][]{
            {uno, due, tre},
            {quattro, cinque, sei},
            {sette, otto, nove}
    };

    static void drawGriglia(){

        AI = new AIController();

        panel.setLayout(layout);
        panel.setBackground(Color.WHITE);

        JPanel jp = new JPanel();
        jp.setBackground(Color.BLACK);
        Dimension d = new Dimension(480,3);
        jp.setPreferredSize(d);

        JPanel jj = new JPanel();
        jj.setBackground(Color.BLACK);
        jj.setPreferredSize(d);

        d = new Dimension(3, 480);

        JPanel hamilton = new JPanel();
        hamilton.setBackground(Color.BLACK);
        hamilton.setPreferredSize(d);

        JPanel sara = new JPanel();
        sara.setBackground(Color.BLACK);
        sara.setPreferredSize(d);

        panel.add(jp);
        panel.add(jj);
        panel.add(hamilton);
        panel.add(sara);

        layout.putConstraint(SpringLayout.WEST, jp, 10, SpringLayout.WEST, cont);
        layout.putConstraint(SpringLayout.NORTH, jp, 500/3, SpringLayout.NORTH, cont);

        layout.putConstraint(SpringLayout.WEST, jj, 10, SpringLayout.WEST, cont);
        layout.putConstraint(SpringLayout.NORTH, jj, 500/3-10, SpringLayout.SOUTH, jp);

        layout.putConstraint(SpringLayout.WEST, hamilton, 500/3, SpringLayout.WEST, cont);
        layout.putConstraint(SpringLayout.NORTH, hamilton, 10, SpringLayout.NORTH, cont);

        layout.putConstraint(SpringLayout.WEST, sara, 500/3, SpringLayout.WEST, hamilton);
        layout.putConstraint(SpringLayout.NORTH, sara, 10, SpringLayout.SOUTH, cont);

        d = new Dimension(163, 155);

        uno.setPreferredSize(d);
        due.setPreferredSize(d);
        tre.setPreferredSize(d);
        quattro.setPreferredSize(d);
        cinque.setPreferredSize(d);
        sei.setPreferredSize(d);
        sette.setPreferredSize(d);
        otto.setPreferredSize(d);
        nove.setPreferredSize(d);

        panel.add(uno);
        panel.add(due);
        panel.add(tre);
        panel.add(quattro);
        panel.add(cinque);
        panel.add(sei);
        panel.add(sette);
        panel.add(otto);
        panel.add(nove);

        layout.putConstraint(SpringLayout.EAST, uno, 0, SpringLayout.WEST, hamilton);
        layout.putConstraint(SpringLayout.SOUTH, uno, 0, SpringLayout.NORTH, jp);

        layout.putConstraint(SpringLayout.EAST, due, 0, SpringLayout.WEST, sara);
        layout.putConstraint(SpringLayout.SOUTH, due, 0, SpringLayout.NORTH, jp);

        layout.putConstraint(SpringLayout.WEST, tre, 0, SpringLayout.EAST, sara);
        layout.putConstraint(SpringLayout.SOUTH, tre, 0, SpringLayout.NORTH, jp);

        layout.putConstraint(SpringLayout.EAST, quattro, 0, SpringLayout.WEST, hamilton);
        layout.putConstraint(SpringLayout.SOUTH, quattro, 0, SpringLayout.NORTH, jj);

        layout.putConstraint(SpringLayout.EAST, cinque, 0, SpringLayout.WEST, sara);
        layout.putConstraint(SpringLayout.SOUTH, cinque, 0, SpringLayout.NORTH, jj);

        layout.putConstraint(SpringLayout.WEST, sei, 0, SpringLayout.EAST, sara);
        layout.putConstraint(SpringLayout.SOUTH, sei, 0, SpringLayout.NORTH, jj);

        layout.putConstraint(SpringLayout.EAST, sette, 0, SpringLayout.WEST, hamilton);
        layout.putConstraint(SpringLayout.NORTH, sette, 0, SpringLayout.SOUTH, jj);

        layout.putConstraint(SpringLayout.EAST, otto, 0, SpringLayout.WEST, sara);
        layout.putConstraint(SpringLayout.NORTH, otto, 0, SpringLayout.SOUTH, jj);

        layout.putConstraint(SpringLayout.WEST, nove, 0, SpringLayout.EAST, sara);
        layout.putConstraint(SpringLayout.NORTH, nove, 0, SpringLayout.SOUTH, jj);

        uno.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uno = ScriviCasella.scrivi(uno);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(1);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 1");
            }
        });

        due.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                due = ScriviCasella.scrivi(due);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(2);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 2");
            }
        });

        tre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tre = ScriviCasella.scrivi(tre);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(3);

                switch (tmp){
                    case 1:
                        uno = ScriviCasella.scrivi(uno);
                        break;
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 3");
                System.out.println("Next Box: " + tmp);
            }
        });

        quattro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quattro = ScriviCasella.scrivi(quattro);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(4);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 4");
            }
        });

        cinque.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cinque = ScriviCasella.scrivi(cinque);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(5);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 5");
            }
        });

        sei.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sei = ScriviCasella.scrivi(sei);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(6);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 6");
            }
        });

        sette.addActionListener(e -> {
            sette = ScriviCasella.scrivi(sette);
            VincitaController.controllaVincita(matrice);

            AI.setNumero_mosse(AI.getNumero_mosse() + 1);

            Integer tmp = AI.nextBox(7);

            switch (tmp){
                case 2:
                    due = ScriviCasella.scrivi(due);
                    break;
                case 3:
                    tre = ScriviCasella.scrivi(tre);
                    break;
                case 4:
                    quattro = ScriviCasella.scrivi(quattro);
                    break;
                case 5:
                    cinque = ScriviCasella.scrivi(cinque);
                    break;
                case 6:
                    sei = ScriviCasella.scrivi(sei);
                    break;
                case 7:
                    sette = ScriviCasella.scrivi(sette);
                    break;
                case 8:
                    otto = ScriviCasella.scrivi(otto);
                    break;
                case 9:
                    nove = ScriviCasella.scrivi(nove);
                    break;
                default:
                    break;
            }
            System.out.println("Pressed: 7");
        });

        otto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                otto = ScriviCasella.scrivi(otto);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(8);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 8");
            }
        });

        nove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nove = ScriviCasella.scrivi(nove);
                VincitaController.controllaVincita(matrice);

                AI.setNumero_mosse(AI.getNumero_mosse() + 1);

                Integer tmp = AI.nextBox(9);

                switch (tmp){
                    case 2:
                        due = ScriviCasella.scrivi(due);
                        break;
                    case 3:
                        tre = ScriviCasella.scrivi(tre);
                        break;
                    case 4:
                        quattro = ScriviCasella.scrivi(quattro);
                        break;
                    case 5:
                        cinque = ScriviCasella.scrivi(cinque);
                        break;
                    case 6:
                        sei = ScriviCasella.scrivi(sei);
                        break;
                    case 7:
                        sette = ScriviCasella.scrivi(sette);
                        break;
                    case 8:
                        otto = ScriviCasella.scrivi(otto);
                        break;
                    case 9:
                        nove = ScriviCasella.scrivi(nove);
                        break;
                    default:
                        break;
                }
                System.out.println("Pressed: 9");
            }
        });

        cont.add(panel);

    }

}
