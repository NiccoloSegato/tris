package it.aldini.tris;

import javax.swing.*;

public class Main {

    public static JFrame window = new JFrame("Tris");

    public static void main(String[] args) {

        window.setBounds(100, 100, 500, 520);
        Griglia.drawGriglia();
        window.setVisible(true);
        window.setResizable(false);
    }
}
