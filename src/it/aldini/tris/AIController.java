package it.aldini.tris;

import it.aldini.AI.Node;
import it.aldini.AI.Tree;

public class AIController {

    // region ATTRIBUTI
    private Tree tree;
    private Integer numero_mosse = 0;
    // endregion

    // region COSTRUTTORE
    public AIController(){
        tree = new Tree();
        tree.addNode(0, new Node(0, 0));
        tree.addNode(1, new Node(1, 5));
        tree.addNode(1, new Node(2, 2));
        tree.addNode(1, new Node(3, 5));
        tree.addNode(1, new Node(4, 4));
        tree.addNode(1, new Node(5, 0));
        tree.addNode(1, new Node(6, 6));
        tree.addNode(1, new Node(7, 5));
        tree.addNode(1, new Node(8, 8));
        tree.addNode(1, new Node(9, 5));
    }
    // endregion

    // region METODI PUBBLICI
    public Integer nextBox(Integer casella_selected){
        Node tmp = null;
        if (casella_selected == 1){
            tmp = tree.getNode(numero_mosse, casella_selected);
        }
        else{
            tmp = tree.getNode(numero_mosse, casella_selected - 2);
        }
        createNewLevel(tmp);
        return tmp.getCasella_succ();
    }
    // endregion

    // region METODI PRIVATI
    private void createNewLevel(Node nodo_partenza) {

        // region LIVELLO 2
        // Da capire perché 1 corrisponde a 3
        if ((nodo_partenza.getVal() == 3) && (numero_mosse == 1)) {
            tree.addNode(2, new Node(1, 0));
            tree.addNode(2, new Node(2, 3));
            tree.addNode(2, new Node(3, 2));
            tree.addNode(2, new Node(4, 7));
            tree.addNode(2, new Node(5, 0));
            tree.addNode(2, new Node(6, 3));
            tree.addNode(2, new Node(7, 4));
            tree.addNode(2, new Node(8, 4));
            tree.addNode(2, new Node(9, 2));
        }
        // endregion

        // region LIVELLO 3
        if ((nodo_partenza.getVal() == 2) && (numero_mosse == 2)) {
            tree.addNode(3, new Node(1, 0));
            tree.addNode(3, new Node(2, 3));
            tree.addNode(3, new Node(3, 0));
            tree.addNode(3, new Node(4, 7));
            tree.addNode(3, new Node(5, 0));
            tree.addNode(3, new Node(6, 0));
            tree.addNode(3, new Node(7, 4));
            tree.addNode(3, new Node(8, 0));
            tree.addNode(3, new Node(9, 0));
        }

        if ((nodo_partenza.getVal() == 4) && (numero_mosse == 2)) {
            tree.addNode(3, new Node(1, 0));
            tree.addNode(3, new Node(2, 3));
            tree.addNode(3, new Node(3, 2));
            tree.addNode(3, new Node(4, 7));
            tree.addNode(3, new Node(5, 0));
            tree.addNode(3, new Node(6, 0));
            tree.addNode(3, new Node(7, 4));
            tree.addNode(3, new Node(8, 7));
            tree.addNode(3, new Node(9, 0));
        }

        if ((nodo_partenza.getVal() == 9) && (numero_mosse == 2)) {
            tree.addNode(3, new Node(1, 0));
            tree.addNode(3, new Node(2, 3));
            tree.addNode(3, new Node(3, 2));
            tree.addNode(3, new Node(4, 7));
            tree.addNode(3, new Node(5, 0));
            tree.addNode(3, new Node(6, 0));
            tree.addNode(3, new Node(7, 4));
            tree.addNode(3, new Node(8, 7));
            tree.addNode(3, new Node(9, 0));
        }
        // endregion

        // region LIVELLO 4
        if ((nodo_partenza.getVal() == 2) && (numero_mosse == 2)) {
            tree.addNode(4, new Node(1, 0));
            tree.addNode(4, new Node(2, 3));
            tree.addNode(4, new Node(3, 0));
            tree.addNode(4, new Node(4, 7));
            tree.addNode(4, new Node(5, 0));
            tree.addNode(4, new Node(6, 8));
            tree.addNode(4, new Node(7, 4));
            tree.addNode(4, new Node(8, 0));
            tree.addNode(4, new Node(9, 0));
        }

        if ((nodo_partenza.getVal() == 3) && (numero_mosse == 3)) {
            tree.addNode(4, new Node(1, 0));
            tree.addNode(4, new Node(2, 3));
            tree.addNode(4, new Node(3, 0));
            tree.addNode(4, new Node(4, 7));
            tree.addNode(4, new Node(5, 0));
            tree.addNode(4, new Node(6, 8));
            tree.addNode(4, new Node(7, 4));
            tree.addNode(4, new Node(8, 6));
            tree.addNode(4, new Node(9, 0));
        }

        if ((nodo_partenza.getVal() == 3) && (numero_mosse == 4)) {
            tree.addNode(4, new Node(1, 0));
            tree.addNode(4, new Node(2, 3));
            tree.addNode(4, new Node(3, 6));
            tree.addNode(4, new Node(4, 7));
            tree.addNode(4, new Node(5, 0));
            tree.addNode(4, new Node(6, 8));
            tree.addNode(4, new Node(7, 4));
            tree.addNode(4, new Node(8, 6));
            tree.addNode(4, new Node(9, 0));
        }
        // endregion
    }

    // region CREAZIONE LIVELLI
    // endregion
    // endregion

    // region GET & SET
    public Tree getTree() {
        return tree;
    }

    public void setNumero_mosse(Integer numero_mosse) {
        this.numero_mosse = numero_mosse;
    }

    public Integer getNumero_mosse() {
        return numero_mosse;
    }
    // endregion
}
