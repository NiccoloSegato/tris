package it.aldini.trisController;

import it.aldini.tris.Vincita;

import javax.swing.*;

public abstract class VincitaController {

    private static int contatore = 0;

    static public void controllaVincita(JButton[][] matrice){

        contatore++;

        if (matrice[0][0].getText() == matrice[0][1].getText() && matrice[0][1].getText() == matrice[0][2].getText()){
            vittoria(contatore);
        }
        else if (matrice[1][0].getText() == matrice[1][1].getText() && matrice[1][1].getText() == matrice[1][2].getText()){
            vittoria(contatore);
        }
        else if (matrice[2][0].getText() == matrice[2][1].getText() && matrice[2][1].getText() == matrice[2][2].getText()){
            vittoria(contatore);
        }
        else if (matrice[0][0].getText() == matrice[1][0].getText() && matrice[2][0].getText() == matrice[1][0].getText()){
            vittoria(contatore);
        }
        else if (matrice[0][1].getText() == matrice[1][1].getText() && matrice[2][1].getText() == matrice[1][1].getText()){
            vittoria(contatore);
        }
        else if (matrice[0][2].getText() == matrice[1][2].getText() && matrice[2][2].getText() == matrice[1][2].getText()){
            vittoria(contatore);
        }
        else if (matrice[0][0].getText() == matrice[1][1].getText() && matrice[1][1].getText() == matrice[2][2].getText()){
            vittoria(contatore);
        }
        else if (matrice[0][2].getText() == matrice[1][1].getText() && matrice[1][1].getText() == matrice[2][0].getText()){
            vittoria(contatore);
        }

        else if (contatore == 9){
            Vincita.drawPareggio();
        }
    }

    private static void vittoria(int turno){
        String giocatoreVincente;
        if (turno % 2 == 0){
            giocatoreVincente = "P1";
        }
        else {
            giocatoreVincente = "P1";
        }
        Vincita.drawVincita(giocatoreVincente);
    }

}
