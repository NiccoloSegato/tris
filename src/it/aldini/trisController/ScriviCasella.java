package it.aldini.trisController;

import it.aldini.tris.Vincita;

import javax.swing.*;
import java.awt.*;

public abstract class ScriviCasella extends JButton {

    private static Boolean turno = true;    // TRUE è il primo giocatore

    static public JButton scrivi(JButton button){
        if (turno){
            button.setText("X");
            turno = false;
        }
        else {
            button.setText("O");
            turno = true;
        }
        final Font defaultFont = new Font("Comic Sans", Font.PLAIN, 120);
        button.setFont(defaultFont);
        button.setEnabled(false);
        return button;
    }
}
